#from contour_det import *

import copy, time

import numpy as np
from functools import reduce
from example_sudoku import h_sudoks,sudoks, m_sudoks, solutions

"""
    ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
    |(0,0)  (0,1)   (0,2) | (0,3)   (0,4)   (0,5) | (0,6)   (0,7)   (0,8)|
    |(1,0)  (1,1)   (1,2) | (1,3)   (1,4)   (1,5) | (1,6)   (1,7)   (1,8)|
    |(2,0)  (2,1)   (2,2) | (2,3)   (2,4)   (2,5) | (2,6)   (2,7)   (2,8)|
    |―――――――――――――――――――――|―――――――――――――――――――――――|――――――――――――――――――――――|
    |(3,0)  (3,1)   (3,2) | (3,3)   (3,4)   (3,5) | (3,6)   (3,7)   (3,8)|
    |(4,0)  (4,1)   (4,2) | (4,3)   (4,4)   (4,5) | (4,6)   (4,7)   (4,8)|
    |(5,0)  (5,1)   (5,2) | (5,3)   (5,4)   (5,5) | (5,6)   (5,7)   (5,8)|
    |―――――――――――――――――――――|―――――――――――――――――――――――|――――――――――――――――――――――|
    |(6,0)  (6,1)   (6,2) | (6,3)   (6,4)   (6,5) | (6,6)   (6,7)   (6,8)|
    |(7,0)  (7,1)   (7,2) | (7,3)   (7,4)   (7,5) | (7,6)   (7,7)   (7,8)|
    |(8,0)  (8,1)   (8,2) | (8,3)   (8,4)   (8,5) | (8,6)   (8,7)   (8,8)|
    ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
"""

class Solver:
    def __init__(self, sudoku, opt_i=None, opt_N=None, opt=None):
        # int array 9*9. Each value representing numbers in sudoku being solved with zero meaning empty.
        self.sudoku = sudoku

        if opt_i == None:
            self.opt_idx, self.opt_count, self.opt = self.map()
        else:
            self.opt_idx, self.opt_count, self.opt = opt_i, opt_N, opt
        self.solve()

    def solve(self):
        counter = 1
        while True:
            print(f"iteration #{counter}. There are {np.sum(self.sudoku == 0)} empty cells left")
            self.print()
            easy = np.array(np.where(self.opt_count == 1)[0])
            if easy.shape[0] == 0:
                if np.sum(self.sudoku == 0) == 0:
                    break

                print(f"No easy fields. Guessing...")
                sort_order = np.argsort(self.opt_count)
                easy_idx = sort_order[0]
                active_set = self.opt[easy_idx]
                easy_ish_coord = self.opt_idx[easy_idx]
                print(f"Active set {active_set} at {easy_ish_coord}.")

                while len(active_set):
                    popped = active_set.pop()
                    print(f"Trying {popped}.")
                    self.opt_count[easy_idx] -= 1
                    try:
                        copy = np.copy(self.sudoku)
                        copy[easy_ish_coord[0], easy_ish_coord[1]] = popped
                        tmp = Solver(copy)
                        self.sudoku = tmp.sudoku
                        return
                    except:
                        print("This is not the way.")
                raise Exception("Not this way")
                # break
            
            #Insert values that are for sure correct
            self.sudoku[self.opt_idx[easy, 0], self.opt_idx[easy, 1]] = np.array([x.pop() for x in self.opt[easy]])
            self.opt_update(easy)

            self.opt_idx = np.delete(self.opt_idx, easy, axis=0)
            self.opt_count = np.delete(self.opt_count, easy)
            self.opt = np.delete(self.opt, easy)
            #print("opt_count", self.opt_count)

            counter +=  1
        return

    def opt_update(self, coords):
        points = self.opt_idx[coords]
        for x, y in zip(points[:, 0], points[:, 1]):
            val = self.sudoku[x, y]
            to_subtract_row_idx = np.where(self.opt_idx[:, 0] == x)[0]
            to_subtract_col_idx = np.where(self.opt_idx[:, 1] == y)[0]
            [opt_set.discard(val) for opt_set in self.opt[np.union1d(to_subtract_row_idx, to_subtract_col_idx)]]

        for i in range(len(self.opt_count)):
            self.opt_count[i] = len(self.opt[i])

    def map(self):
        row_opt = np.empty((9), dtype=object)
        col_opt = np.empty((9), dtype=object)
        sub_opt = np.empty((3, 3), dtype=object)

        # Check for [1 ... 10) for each column / row
        for idx in range(9):
            temp_row = []
            temp_col = []
            for num in range(1, 10):
                if not num in self.sudoku[idx]:
                    temp_row.append(num)
                if not num in self.sudoku.T[idx]:
                    temp_col.append(num)
            row_opt[idx] = np.array(temp_row, dtype=int)
            col_opt[idx] = np.array(temp_col, dtype=int)

        for row in range(0, 3):
            for col in range(0, 3):
                temp_sub = []
                for num in range(1, 10):
                    if not num in self.sudoku[3*row:3*row+3, 3*col:3*col+3]:
                        temp_sub.append(num)
                sub_opt[row, col] = np.array(temp_sub, dtype=int)

        idx = np.array(np.where(self.sudoku == 0)).T
        row = row_opt[idx[:, 0]]
        col = col_opt[idx[:, 1]]
        sub = sub_opt[np.int_(idx[:, 0]/3), np.int_(idx[:, 1]/3)]

        opt = np.empty(row.shape[0], dtype=object)
        N_opt = np.empty(row.shape[0], dtype=int)

        for a in range(0, row.shape[0]):
            opt[a] = set(reduce(np.intersect1d, (row[a], col[a], sub[a])))
            N_opt[a] = len(opt[a])
        return [idx, N_opt, opt]

    def print(self):
        for a in self.sudoku:
            print(a)
        print('\n')


def cmp(sud1, sud2):
    ctr = 0
    for a, b in zip(sud1.ravel(), sud2.ravel()):
        if a != b:
            ctr += 1
    print("The 2 Sudokus differ in {} fields.".format(ctr) if ctr else "The sudokus are identical")


#images = ["data/01_-30_deg_sudoku.png", "data/02_sudoku.jpg", "data/03_sudoku.jpg", "data/04_sudoku.jpg", "data/05_sudoku.jpg", "data/06_sudoku.jpg", "data/07_sudoku.jpg", "data/08_sudoku.jpg", "data/09_sudoku.jpg", "data/10_sudoku.jpg", "data/11_sudoku.jpg", "data/12_sudoku.jpg", "data/13_sudoku.jpg", "data/14_sudoku.png", "data/15_sudoku.jpg", "data/16_sudoku.png"]
#sudok = detectObjects(images[-1])
#sudok = sudoks[2]
#print("Initial difference:")

#start = time.time()
#a = Solver(sudok)
#stop = time.time()
#a = Solver(m_7_sudoku['task'])
#n = 3
#a = Solver(m_sudoks[n]['task'])
#cmp(a.sudoku, m_sudoks[n]['solution'])
n=0
task = f"data/h_{n}_sudoku.png"
solution = f"data/sol_h_{n}_sudoku.png"
a = Solver(h_sudoks[n]['task'])
cmp(a.sudoku, h_sudoks[n]['solution'])
#print({'task': detectObjects(task), 'solution': detectObjects(solution)}, sep=',')
#for i in range(0, 7):
#    a = Solver(m_sudoks[i]['task'])
#    cmp(a.sudoku, m_sudoks[i]['solution'])
    #task = f"data/m_{a}_sudoku.png"
    #solution = f"data/sol_m_{a}_sudoku.png"
    #print({'task': detectObjects(task), 'solution': detectObjects(solution)}, sep=',')
#cmp(a.sudoku, solutions[0])

#print("It took {}s.".format(stop-start))
