# import the necessary packages

import cv2 as cv
import pytesseract as tess
import numpy as np 
import time
import imutils
import matplotlib.pyplot as plt
from scipy import ndimage
from scipy.signal import savgol_filter


from dataclasses import dataclass
from PIL import Image, ImageEnhance

def plotter(ax, data_x, data_y, param_dict = {}):
    # ["xlabel", "ylabel", "title", "xlim", "ylim"]
    funcs = {"xlabel": ax.set_xlabel, "ylabel": ax.set_ylabel, "title": ax.set_title, "xlim": ax.set_xlim, "ylim": ax.set_ylim}
    if not "xlim" in param_dict: param_dict["xlim"] = (data_x[0], data_x[-1])
    for parm in funcs.keys():
        if parm in param_dict:
            funcs[parm](param_dict.pop(parm))
    
    ax.plot(data_x, data_y, **param_dict)

def creator(title="Some title", data_x = None, data_y= None, param_dict = {}, n=1):
    fig, ax = plt.subplots(n)
    fig.suptitle(title)
    if n == 1 and not data_x is None and not data_y is None:
        plotter(ax, data_x, data_y, param_dict)
    #fig.show()
    return fig, ax

deg2rad = lambda a : a*np.pi/180
rad2deg = lambda a : a*180/np.pi

#!Uncomment the next line if on windows
#tess.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

"""
class Line:
    def __init__(self, startPoint, stopPoint):
        pos: np.empty((2), dtype=(('x', float), ('y', float)))  #points of beginning and end
        len: float
        s: np.empty((1), dtype=(('pts', int, 2), ('x', float), ('y', float), ('k', float), ('ang', float)))    #Direction vector
"""

def norm(arr):
    max = arr.max()
    return arr/max

class sudokuSolver:
    def __init__(self, filename):
        self.filename = filename
        self.cv_img = cv.imread(filename)
        self.PILimg = Image.open(filename)
        self.lines = self.lineDetect()

    def getLines(self, img = None):
        if type(img) != None:
            self.cv_img = img

        gray = cv.cvtColor(cv.GaussianBlur(self.cv_img, (5, 5), cv.BORDER_DEFAULT), cv.COLOR_BGR2GRAY)
        edges = cv.Canny(gray, 75, 150)
        ln = cv.HoughLinesP(edges, 1, np.pi/180, 30,minLineLength = 20, maxLineGap = 10)
        allLn = np.empty(len(ln), dtype=[('pts', int, 4), ('s', int, 2), ('n', int, 2), ('k', int), ('ang', float), ('len', float)])
        
        allLn['pts'] = ln[:, 0, :]
        allLn['s'][:, 0] = allLn['pts'][:,0]-allLn['pts'][:,2]  #The direction vector
        allLn['s'][:, 1] = allLn['pts'][:,1]-allLn['pts'][:,3]
        allLn['n'][:, 0] = allLn['s'][:,1]      #The normal vector (perpendicular to the line)
        allLn['n'][:, 1] = -allLn['s'][:,0]

        #k for the general equation of a line in: ax + by + k = 0
        allLn['k'] = -(allLn['n'][:,0]*allLn['pts'][:,0] + allLn['n'][:,1]*allLn['pts'][:,1])
        allLn['len'] = np.sqrt(allLn['s'][:, 0]**2 + allLn['s'][:, 1]**2)   #The length of the detected line segment
        allLn['ang'] = np.arctan2(allLn['s'][:, 0], allLn['s'][:, 1])+np.pi/2
        allLn.sort(order='ang')
        return allLn

    def detectRowCol(self, vals, m):
        # calculate for y = 0
        # m = 0 detects columns, m = 1 detects rows

        n = int(not m)
        x = np.abs(np.array(np.rint((vals['k'] - vals['n'][:, n]*vals['pts'][:, n])/vals['n'][:, m]), dtype = int))
        hist = np.zeros(self.rotated.shape[m])
        hist[x] += vals['len']
        hist = norm(hist)
        filtered = norm(savgol_filter(hist, 49, 2))

        xs = np.arange(hist.shape[0])
        dXs = filtered[1:] - filtered[0:-1]
        plot, ax = creator(f"Data in {'columns' if m == 0 else 'rows'}", xs[1:], dXs, {"xlabel": "x position", "ylabel": "value", "marker": "v", "linestyle": ":"})
        plotter(ax, xs, hist, {"label": "histogram", "marker": "^", "linestyle": "-."})
        plotter(ax, xs, filtered, {"label": "filtered", "marker": ">", "linestyle": "--"})

        #plt.plot(xs, filtered)
        plt.show()

    def detRows(self, horz):
            self.detectRowCol(horz, 1)

    def detCols(self, vert):
        self.detectRowCol(vert, 0)

    def lineDetect(self, cols=9, angBins=120):
        start = time.time()

        allLn = self.getLines(self.cv_img) #detect all lines in the picture
        hist, binEdges = np.histogram(allLn['ang'], bins=angBins)   #Create histogram from line angles
        nz = np.nonzero(hist > sum(hist)/10)[0]     #Choose those intervals that wrap more than 10 % of lines

        susp = np.empty(len(nz), dtype=[('ang', float), ('abs', float)])
        susp['ang'] = (binEdges[nz] + binEdges[nz+1])/2     #Vector containing angles of the corresponding lines
        susp['abs'] = np.abs(susp['ang'])   #Vector containing absolute values of angles of the corresponding lines
        susp.sort(order='abs')  #Sorting array by absolute value of angle 
        
        phi = rad2deg(susp['ang'][0])   #Choosing an angle of the smallest abs value regardless of orientation

        self.rotated = imutils.rotate_bound(self.cv_img, phi)

        allLinesRot = self.getLines(self.rotated)

        for a, b in zip(allLinesRot['pts'][:, 0:2], allLinesRot['pts'][:, 2:]):
            #printing detected lines on the original picture (optional)
            cv.line(self.cv_img, tuple(a), tuple(b), (0, 0, 128), 1)
        horizontal = list()
        vertical = list()

        for a in allLinesRot:
            if abs(a['ang']) < deg2rad(10):
                horizontal.append(a)
            elif abs(a['ang']) > deg2rad(80) and abs(a['ang']) < deg2rad(100):
                vertical.append(a)
        horz = np.array(horizontal)
        vert = np.array(vertical)

        #self.detCols(vert)
        #self.detRows(horz)

        horz.sort(order='k')
        vert.sort(order='k')

        #print('Horz', horz, '\nVert', vert)
        stop = time.time()

        cv.imshow("Rotated image with detected lines", self.cv_img)
        #plt.scatter(rad2deg(allLinesRot['ang']), rad2deg(allLinesRot['ang']))
        #plt.legend()
        #plt.show()
        cv.waitKey(0)
        cv.destroyAllWindows()
        
        print('Operation took: {} s'.format(np.round(stop-start, 2)))
        #cv.imwrite('rotated.png', rotated)
        return allLinesRot

def sortLen(e):
    return e.len

def sortAngle(e):
    return np.atan(e.s[1]/e.s[0])

def lineAt(line, Coord, imSize):
    temp = line.k*Coord[0] + line.q
    if(temp >= 0 and temp < imSize[1]):
        res = (Coord[0], line.k*Coord[0] + line.q)
    else:
        res = ((Coord[1] - line.q) / line.k, Coord[1])


#imgCv = cv.imread("data/AB.png", 0)
#img_new = Image.fromarray(edges)
#ret,thresh1 = cv.threshold(imgCv,127,255,cv.THRESH_BINARY)

#plt_image = cv.cvtColor(thresh1, cv.COLOR_BGR2RGB)
#imgplot = plt.imshow(plt_image)
#plt.legend()
#plt.show()

#tesseract_config = '--psm 10 -c outputbase digits'     #tessedit_char_whitelist=123457890'
#extData = tess.image_to_data(thresh1, config=tesseract_config, output_type=tess.Output.DICT)
#print(extData)

#suso = sudokuSolver('data/01_+30_deg_sudoku.png')
suso = sudokuSolver('data/02_sudoku.jpg')
