import multiprocessing 
import time 
   
def printNum(id):
    for a in range(int(1e7)):
        b=a
        #if (a%100000 == 0): print(f"inside {id}, num is {a}")
    print(f"{id} is done")
    return [id['val'], id['val'], id['val']]
  
def square(x): 
    return x * x 

values=[{"val": 1}, {"val": 2}, {"val": 3}, {"val": 4}, {"val": 5}, {"val": 6}, {"val": 7}, {"val": 8}, {"val": 9}, {"val": 10}, {"val": 11}, {"val": 12}, {"val": 13}]

pool = multiprocessing.Pool(processes=4) 
results = pool.map(printNum, values)
print(results)