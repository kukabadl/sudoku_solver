import numpy as np
import cv2 as cv
import easyocr

from random import randint as rnd
from time import time
from PIL import Image, ImageEnhance
from numpy.lib.function_base import average
import multiprocessing 
from pprint import pprint

reader = easyocr.Reader(['en'])
polyArea = lambda pts : 1/2*np.abs(np.dot(pts[:, 0],np.roll(pts[:, 1],1))-np.dot(pts[:, 1],np.roll(pts[:, 0],1)))

def newSize(img, maxDim = 1500):
    scaleFactor = max(img.shape)/maxDim
    return (int(img.shape[1]/scaleFactor),int(img.shape[0]/scaleFactor))


## Available statuses:
# 'sus'     OCR got numbers but it is longer than 1
# 'ok'      OCR got one letter and it is a digit
# 'empty'   OCR got nothing
# 'error'   during ocr exception occured or extracted string cannot be interpreted as integer
# 'skipped' area in question was blank and therefore ocr was not performed
def ext(value):
    start = time()
    # Calculate black to white ratio
    bAllRatio = np.sum(value['img'] > 0)/(value['img'].shape[0]*value['img'].shape[1])
    certainty = 0
    # Detect squares that aren't empty and perform OCR
    if bAllRatio < 0.99:
        try:
            val = reader.recognize(value['img'], allowlist = '0123456789')[0]
            if len(val) == 3:
                vl = int(val[1])
                status = 'ok'
                certainty = val[2]
                if len(val[1]) > 1:
                    vl = int(val[1][-1])
                    status = 'sus'
            else: 
                vl = 0
                status = 'empty'
                certainty = 1
        except:
            if val != '':
                #cv.imshow('Digit recognition', val)
                #cv.waitKey(0)
                #cv.destroyAllWindows()
                status = 'error'
            vl = 0

    # If square empty, assign zero
    else: 
        vl = 0
        status = 'skipped'
        certainty = 1
    stop = time()

    # Print stats
    #print(f"extracted={vl} in {np.round(1000*(stop-start))}, with ratio of {np.round(bAllRatio,3)} and certainty of {certainty}")
    return {'value': vl, 'status': status, 'certainty': certainty}

def calc_center(a, b, offset = 0):
    return np.array([int((a[0]+b[0])/2+offset), int((a[1]+b[1])/2+offset)])

def detectObjects(imgName, debug=False):
    origImage = cv.imread(imgName)
    if debug: cv.imshow('original', origImage)
    #maxDim = 1000
    #scaleFactor = max(origImage.shape)/maxDim
    #newSize = (int(origImage.shape[1]/scaleFactor),int(origImage.shape[0]/scaleFactor))

    imgGry = cv.cvtColor(origImage, cv.COLOR_BGR2GRAY)
    
    imgBlurred = cv.medianBlur(imgGry,3)
    

    ret , imgThresholded = cv.threshold(imgBlurred, 127, 255, 0)
    if debug: 
        cv.imshow('gray', imgGry)
        cv.imshow('blurred', imgBlurred)
        cv.imshow('thresholded', imgThresholded)
    #img = cv.resize(imgThresholded, dsize=newSize, interpolation=cv.INTER_CUBIC)
    #scaledColoredImg = cv.resize(origImage, dsize=newSize, interpolation=cv.INTER_CUBIC)
    img = imgThresholded
    scaledColoredImg = img

    empty = np.zeros(scaledColoredImg.shape, np.uint8)
    empty2 = np.zeros(scaledColoredImg.shape, np.uint8)

    contours, hierarchy = cv.findContours(img, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
    counter = 0
    contsMatrix = np.empty(int(len(contours)), dtype=[('approx', int, (4,2)), ('area', float),('thAppA/actA', float), ('val', np.uint8), ('colour', object), ('labelPos', object), ('hrIdx', int), ('hr', int, 4)])

    if debug: print("\nthA/actA", "thAppA/actA", "ThA/AppA", "thArea/thAApp", sep='\t')
    contourList = []
    idx = 0
    for contour, hr in zip(contours, hierarchy[0]):
        epsilon = 0.1*cv.arcLength(contour,True)
        approx = cv.approxPolyDP(contour, epsilon, True)
        x, y , w, h = cv.boundingRect(approx)

        if  w*h > 250 and len(approx) == 4:
            approxArray = np.vstack(approx).squeeze()
            counter += 1
            colour = (rnd(30,255), rnd(30,255), rnd(30,255))
            
            cv.drawContours(empty2, [contour], 0, counter, cv.FILLED)

            s = time()
            area = np.count_nonzero(empty2[y:y+h, x:x+w] == counter)
            e = time()
            a_t = np.round(10e3*(e-s), 3)

            s = time()
            thArea = polyArea(np.vstack(contour).squeeze())
            e = time()
            thA_t = np.round(10e3*(e-s), 3)

            s = time()
            thAreaApprox = polyArea(approxArray)
            e = time()
            thAA_t = np.round(10e3*(e-s), 3)
            contsMatrix[counter-1] = approxArray, area, np.abs(np.round(100 - 100*thAreaApprox/area, 1)), counter, tuple(colour), tuple((x,y)), idx, hr
            if debug: print(np.round(100 - 100*thArea/area, 1), np.round(100 - 100*thAreaApprox/area, 1), np.round(100*thArea/thAreaApprox, 1), sep='\t\t')
        idx += 1
    if debug: cv.imshow('With contours', empty2)
    if debug: print(f"detected {counter} objects")
    sorted = np.sort(contsMatrix[:counter], order=['area'])[::-1]

    parents = sorted[np.argsort(sorted['hr'][:, -1])]
    uniq = np.setdiff1d(np.unique(parents['hr'][:, 3 ]), -1, assume_unique=True)
    sudok = -1
    for a in uniq:
        elsOfValue = np.where(parents['hr'][:, 3] == a)
        parentIdx = np.argmax(parents['hrIdx'] == a)

        areaOfEls = np.sum(parents['area'][elsOfValue])
        elsCount = np.sum(parents['hr'][:, 3 ] == a)

        parArea = parents['area'][parentIdx]
        areaRatio = areaOfEls / parArea
        if areaRatio > 0.8 and elsCount > 5:
            sudok = parentIdx
            if debug: print(f"parent id is {a}, it's area ratio {areaRatio} (consists of {elsCount} children)")

    for cont in sorted:
       if cont['thAppA/actA'] < 10:
            cv.drawContours(empty, [cont['approx']], 0, cont['colour'], cv.FILLED)
            cv.putText(empty, f"{cont['thAppA/actA']}", cont['labelPos'], cv.FONT_HERSHEY_COMPLEX, 0.5, cont['colour'])
    
    if sudok >= 0:
        appr = parents[sudok]['approx']
        avg = np.average(appr, axis=-2)

        rel = avg - appr
        angles = np.arctan2(rel[:, 1], -rel[:, 0])
        sortOrder = np.argsort(angles)
        A, B, C, D = appr[sortOrder, :]

        #cv.circle(img, center, radius, colour, thickness, line_type)
        cv.circle(empty, tuple(A), 30, (255, 255, 255), -1, cv.LINE_8)
        cv.circle(empty, tuple(B), 30, (255, 255, 255), -1, cv.LINE_8)
        cv.circle(empty, tuple(C), 30, (255, 255, 255), -1, cv.LINE_8)
        cv.circle(empty, tuple(D), 30, (255, 255, 255), -1, cv.LINE_8)

        # Output size
        outDim = 720

        pts1 = np.float32(appr[sortOrder, :])

        # Generate points of the resulting square and specify size
        pts2 = np.float32([[0,1],[1, 1],[1, 0],[0,0]])
        pts2 *= outDim

        M = cv.getPerspectiveTransform(pts1,pts2)

        global dstData
        dstEmpty = cv.warpPerspective(empty, M, (outDim, outDim))
        dstData = cv.warpPerspective(scaledColoredImg, M, (outDim, outDim))
        if debug: cv.imshow('warped', dstData)
        # Epsilon is 10 % of the size of a square
        eMult = 1.7
        eAfter = np.array([eMult, 1, 1, eMult, 1, 1, eMult, 1, 1], dtype=int) * int(0.1*outDim/9)
        eBefore = np.array([1, 1, eMult, 1, 1, eMult, 1, 1, eMult], dtype=int) * int(0.1*outDim/9)

        lims = np.linspace(0, outDim, 10, dtype=int, endpoint=True)
        after = lims[:-1] + eAfter
        before = lims[1:] - eBefore

        bounding_boxes = []
        rgb_image = cv.cvtColor(dstData, cv.COLOR_GRAY2BGR)

        st = time()
        for aBase, bBase in zip(after, before):
            for a, b in zip(after, before):
                inp = {"D": (a, aBase), "B": (b, bBase), "img": dstData[aBase:bBase, a: b], "dataImg": dstData}
                bounding_boxes.append({**inp, **ext(inp)})
                r = bounding_boxes[-1]

                colour = (0, 0, 0)
                if r['status'] == 'sus':
                    colour = (180, 180, 0)
                elif r['status'] == 'ok':
                    colour = (0, 255, 0)
                elif r['status'] == 'error':
                    colour = (255, 0, 0)
                elif r['status'] == 'skipped':
                    continue
                elif r['status'] == 'empty':
                    colour = (255, 255, 255)
                    continue
                colour = (colour[::-1])
                if debug: print(f"colour = {colour}, status = {r['status']}")
                # Write OCRd data in the image for faster evaluation
                cv.putText(rgb_image, f"{r['value']}", (int((r['D'][0]+r['B'][0])/2+7/380*outDim), int((r['D'][1]+r['B'][1])/2+7/380*outDim)), cv.FONT_HERSHEY_SIMPLEX, 0.5/380*outDim, colour, int(np.round(1/300*outDim)))
                cv.rectangle(rgb_image, r['D'], r['B'], (127, 127, 127), 2, cv.LINE_8)

        


        # Multiprocessing
        """
        pool = multiprocessing.Pool(processes=3)
        results = pool.map(ext, bounding_boxes)
        print(f"It took {np.round(time()-st, 3)} s on multicore")
        """

        if debug: cv.imshow("rectangled", rgb_image)

        # Display and evaluate OCR
        numpy_horizontal_concat = np.concatenate((dstData, dstEmpty), axis=1) #, cv.cvtColor(img, cv.COLOR_GRAY2BGR)
        interp = cv.resize(numpy_horizontal_concat, dsize=newSize(numpy_horizontal_concat), interpolation=cv.INTER_CUBIC)
        cv.namedWindow('eval_ocr')
        cv.setMouseCallback('eval_ocr', sel_defect)
        cv.imshow('eval_ocr', rgb_image)
        while True:
            key = cv.waitKey(0)
            if key == ord('q'):
                break
            elif key == ord('r'):
                raise Exception("Terminating process.") 
            else:
                try:
                    # Click desired digit if one at the clicked position is not correct
                    num = int(chr(key))
                    print(f"putting {num} at {ref_pt}")
                    for p in bounding_boxes:
                        if (ref_pt[0] > p['D'][0] and ref_pt[1] > p['D'][1]) and (ref_pt[0] < p['B'][0] and ref_pt[1] < p['B'][1]):
                            print(f"{ref_pt} fits inside {p['D']}, {p['B']}")
                            cv.putText(rgb_image, chr(key), ref_pt, cv.FONT_HERSHEY_SIMPLEX, 0.5/380*outDim, (0, 255, 0), int(np.round(1/300*outDim)))
                            cv.imshow('eval_ocr', rgb_image)
                            p['value'] = num
                except:
                    print(f"\"{chr(key)}\" is not a digit and can\'t be used.")
        cv.destroyAllWindows()
        return np.array([a['value'] for a in bounding_boxes]).reshape((9,9))

# Mouse click on image callback function which records the click position
def sel_defect(event, x, y, flags, param):
    global ref_pt
    if event == cv.EVENT_LBUTTONDOWN:
        ref_pt = (x, y)
