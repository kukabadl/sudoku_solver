To determine how much the picture is rotated I will look at all the lines and sort them by the angle.
Next I will create the smallest interval able to accomodate all angles in the sorted array.
I will split this interval into n/3 small ones where n is the total number of lines and calculate how many lines are in each subinterval.
    The "density" 