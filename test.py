import matplotlib.pyplot as plt
from scipy import ndimage
import numpy as np

def plotter(ax, data_x, data_y, param_dict = {}):
    # ["xlabel", "ylabel", "title", "xlim", "ylim"]
    funcs = {"xlabel": ax.set_xlabel, "ylabel": ax.set_ylabel, "title": ax.set_title, "xlim": ax.set_xlim, "ylim": ax.set_ylim}
    if not "xlim" in param_dict: param_dict["xlim"] = (data_x[0], data_x[-1])
    for parm in funcs.keys():
        if parm in param_dict:
            funcs[parm](param_dict.pop(parm))
    
    ax.plot(data_x, data_y, **param_dict)

def creator(title="Some title", data_x = None, data_y= None, param_dict = {}, n=1):
    fig, ax = plt.subplots(n)
    fig.suptitle(title)
    if n == 1 and not data_x is None and not data_y is None:
        plotter(ax, data_x, data_y, param_dict)
    return fig, ax

x = np.arange(30)
    
plt.plot(x, x)  
plt.show()  